/**
 * Init function loads tables, registers settings, and loads templates
 */
Hooks.once("init", () => {

  game.settings.register("gurps4e", "systemMigrationVersion", {
    name: "System Migration Version",
    scope: "world",
    config: true,
    type: Number,
    default: 1
  });

  // Register initiative rule
  game.settings.register("gurps4e", "initiativeRule", {
    name: "SETTINGS.InitRule",
    hint: "SETTINGS.InitHint",
    scope: "world",
    config: true,
    default: "default",
    type: String,
    choices: {
      "default": "SETTINGS.InitDefault",
      "house": "SETTINGS.InitHouse",
      "house2": "SETTINGS.InitHouse2"
    },
    onChange: rule => _setGurps4eInitiative(rule)
  });
  _setGurps4eInitiative(game.settings.get("gurps4e", "initiativeRule"));

  function _setGurps4eInitiative(initMethod) {
    let formula;
    switch (initMethod) {
      case "default":
      formula = "(1d100 - 1) / 1000000 + @primaryAttributes.dexterity.value / 10000 + @secondaryAttributes.bs.value";
      break;

      case "house":
      formula = "2d100 / 1000 + @primaryAttributes.dexterity.value / 1000 + @secondaryAttributes.bs.value";
      break;

      case "house2":
      formula = "(1d750 / 1000) -.251 + @secondaryAttributes.bs.value";
      break;
    }

    let decimals = (initMethod == "default") ? 6 : 3;
    CONFIG.Combat.initiative = {
      formula: formula,
      decimals: decimals
    }
  }

  // Register Showing Roll Flavour
  game.settings.register("gurps4e", "showRollFlavour", {
    name: "SETTINGS.ShowRollFlavour",
    hint: "SETTINGS.ShowRollFlavourHint",
    scope: "world",
    config: true,
    default: true,
    type: Boolean
  });

  // Register Showing Roll Trait
  game.settings.register("gurps4e", "showRollTrait", {
    name: "SETTINGS.ShowRollTrait",
    hint: "SETTINGS.ShowRollTraitHint",
    scope: "world",
    config: true,
    default: false,
    type: Boolean
  });

  // Register Using Conditional Injury
  game.settings.register("gurps4e", "useConditionalInjury", {
    name: "SETTINGS.UseConditionalInjury",
    hint: "SETTINGS.UseConditionalInjuryHint",
    scope: "world",
    config: true,
    default: false,
    type: Boolean
  });

  // Register Using 1/2 FP Increments
  game.settings.register("gurps4e", "useHalfFPIncrements", {
    name: "SETTINGS.UseHalfFPIncrements",
    hint: "SETTINGS.UseHalfFPIncrementsHint",
    scope: "world",
    config: true,
    default: false,
    type: Boolean
  });

  // Register Hiding Test Data
  game.settings.register("gurps4e", "hideTestData", {
    name: "SETTINGS.HideTestData",
    hint: "SETTINGS.HideTestDataHint",
    scope: "world",
    config: true,
    default: true,
    type: Boolean
  });

  // Pre-load templates
  loadTemplates([
    "systems/gurps4e/templates/actor/actor-sheet.html",
    "systems/gurps4e/templates/chat/chat-message.html",
    "systems/gurps4e/templates/chat/dialog-constant.html",
    "systems/gurps4e/templates/chat/test-card.html",
    "systems/gurps4e/templates/chat/chat-command-display-info.html",
    "systems/gurps4e/templates/item/item-sheet.html"
  ]);
 
});