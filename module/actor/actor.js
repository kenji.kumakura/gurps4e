/**
 * Extend the base Actor entity by defining a custom roll data structure which is ideal for the Simple system.
 * @extends {Actor}
 */
export class gurpsActor extends Actor {

  /**
   * Augment the basic actor data with additional dynamic data.
   */
  prepareData() {

    super.prepareData();

    // the refreshed/cleaned data based on the template
    const actorData = this.data;

    actorData.data.useConditionalInjury = game.settings.get("gurps4e", "useConditionalInjury");
    actorData.data.showRollFlavour = game.settings.get("gurps4e", "showRollFlavour");
    actorData.data.showRollTrait = game.settings.get("gurps4e", "showRollTrait");

    // deal with all of the calculations and update all values in dynamic
    this._prepareCharacterData(actorData);
  }

  /**
   * Handle how changes to a Token attribute bar are applied to the Actor.
   * This allows for game systems to override this behavior and deploy special logic.
   * @param {string} attribute    The attribute path
   * @param {number} value        The target attribute value
   * @param {boolean} isDelta     Whether the number represents a relative change (true) or an absolute change (false)
   * @param {boolean} isBar       Whether the new value is part of an attribute bar, or just a direct value
   * @return {Promise}
   */
  async modifyTokenAttribute(attribute, value, isDelta = false, isBar = true) {
    const current = getProperty(this.data.data, attribute);
    if (isBar) {
      if (isDelta) value = Math.clamped(current.min, Number(current.value) + value, current.max);

      // TODO: insert a call to the status check method
      this.setConditions(value, attribute);

      return this.update({ [`data.${attribute}.value`]: value });
    } else {
      if (isDelta) value = Number(current) + value;

      // TODO: insert a call to the status check method
      this.setConditions(value, attribute);

      return this.update({ [`data.${attribute}`]: value });
    }
  }

  slugify(text) {
    return text
      .toString()                     // Cast to string
      .toLowerCase()                  // Convert the string to lowercase letters
      .normalize('NFD')       // The normalize() method returns the Unicode Normalization Form of a given string.
      .trim()                         // Remove whitespace from both sides of a string
      .replace(/\s+/g, '-')           // Replace spaces with -
      .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
      .replace(/\-\-+/g, '-');        // Replace multiple - with single -
  }

  /**
   * Replace referenced data attributes in the formula with the syntax `@attr` with the corresponding key from
   * the provided `data` object.
   * @param {String} formula    The original formula within which to replace
   * @private
   */
  _replaceData(formula) {
    let dataRgx = new RegExp(/@([a-z.0-9_\-]+)/gi);
    return formula.replace(dataRgx, (match, term) => {
      let value = getProperty(this.data.data.dynamic, `${term}.value`);
      return (value) ? String(value).trim() : "0";
    });
  }
  /**
 * Prepare minchar type specific data
 * 
 */
  _prepareCharacterData(actorData) {

    // make a copy of the actor items
    const actoritems = actorData.items;

    // set up a spot to store copies of primary attributes
    actorData.data.primaries = {};
    let previousLayer = [];
    let nextLayer = [];


    // inspect the Pools to see if they need to be added or removed
    for (const item of actoritems) {
      switch (item.type) {
        case "Pool": {
          let itemID = item.data.abbr.toLowerCase();
          if (item.data.inHeader) { // checked, write it
            actorData.data.tracked[itemID] = {
              id: item._id,
              abbr: item.data.abbr,
              name: item.name,
              state: item.data.state,
              min: parseInt(item.data.min),
              value: parseInt(item.data.value),
              max: parseInt(item.data.max)
            };
          } else { // unchecked, remove 
            delete actorData.data.tracked[itemID];
          }
          break;
        }
        // write these items to dynamic
        case "Melee-Attack":
        case "Ranged-Attack":
        case "Defence":
        case "Rollable": {

          let itemID = this.slugify(item.name);
          actorData.data.dynamic[itemID] = {
            name: item.name,
            value: parseInt(item.data.value),
          };
          break;
        }
        case "Primary-Attribute": {

          // write name and value of the item to dynamic
          let itemID = this.slugify(item.data.abbr);
          actorData.data.dynamic[itemID] = {
            name: item.data.abbr,
            value: parseInt(item.data.value),
          };

          // write the item copy to primaries so you may modifiy it later
          actorData.data.primaries[itemID] = item;

          // add the term to the layer
          nextLayer.push(itemID);
          break;
        }
      }
    }

    // modify primary attributes as necessary
    for (const item of actoritems) {

      if (item.data.primary && item.data.inEffect) {
        const cases = item.data.targets.split(",").map(word => word.trim().toLowerCase());
        for (const target of cases) {
          if (actorData.data.dynamic[target]) {

            // write to dynamic
            actorData.data.dynamic[target].value += item.data.value;

            // write to primaries
            let origitem = actorData.data.primaries[target];
            origitem.data.value = parseInt(origitem.data.value) + item.data.value;
          }
        }
      } else if (item.data.formula && !isNaN(parseInt(item.data.formula))) {

        let dynID = this.slugify(item.name);

        // write to Actor Item
        item.data.value = parseInt(item.data.formula);

        // write to dynamic
        actorData.data.dynamic[dynID].value = item.data.value;

        // add the term to the layer
        nextLayer.push(dynID);
      }
    }

    // calculate the remaining formulae one layer at a time
    while (nextLayer.length !== 0) {
      previousLayer = nextLayer;
      nextLayer = [];

      for (const item of actoritems) {

        //ignore items with no formula
        if (!item.data.formula) continue;

        // formula is a number and is already processed
        if (!isNaN(parseInt(item.data.formula))) continue;

        for (const target of previousLayer) {

          // if this formula refers to an item in the previous layer then process it
          if (item.data.formula.includes(target)) {

            let dynID = this.slugify(item.name);

            // write to Actor Item
            item.data.value = Math.round(eval(this._replaceData(item.data.formula).replace(/[^-()\d/*+.]/g, '')));

            // write to dynamic
            actorData.data.dynamic[dynID].value = item.data.value; // this pushes a new value to the target, not the item

            // put this item into the next layer
            nextLayer.push(dynID);
          }
        }
      }
    }

  }

  setConditions(newValue, attrName) {
    var attrValue;
    var attrMax;
    var attrState;
    var gurpsItem;
    let tracked = this.data.data.tracked;
    let attr = attrName.split('.')[2];

    if (attr === "hp") { // Hit points update

      // Assign the variables
      if (attrName.includes('.max')) {
        attrMax = newValue;
        attrValue = tracked.hp.value;
      } else {
        attrValue = newValue;
        attrMax = tracked.hp.max;
      }
      let ratio = attrValue / attrMax;
      // set the limits
      switch (Math.trunc(ratio)) {
        case 0: {
          if (ratio <= 0) { // collapse
            attrState = '[C]';
            break;
          } else if (attrValue < (attrMax / 3)) { // reeling
            attrState = '[R]';
            break;
          }
          // healthy, no break
        }
        case 1: { // healthy
          attrState = '[H]';
          break;
        }
        case -1: { // death check at -1
          attrState = '[-X]';
          break;
        }
        case -2: { // death check at -2
          attrState = '[-2X]';
          break;
        }
        case -3: { // death check at -3
          attrState = '[-3X]';
          break;
        }
        case -4: { // death check at -4
          attrState = '[-4X]';
          break;
        }
        default: { // dead
          attrState = '[DEAD]';
          break;
        }
      }

    } else if (attr === "fp") { // Fatigue points update

      // Assign the variables
      if (attrName.includes('.max')) {
        attrMax = newValue;
        attrValue = tracked.fp.value;
      } else {
        attrValue = newValue;
        attrMax = tracked.fp.max;
      }
      let ratio = attrValue / attrMax;
      // set the limits
      switch (Math.trunc(ratio)) {
        case 0: {
          if (ratio <= 0) { // collapse
            attrState = '[C]';
            break;
          } else if (attrValue < (attrMax / 3)) { // tired
            attrState = '[T]';
            break;
          }
          // fresh, no break
        }
        case 1: { // fresh
          attrState = '[F]';
          break;
        }
        default: { // unconscious
          attrState = '[UNC]';
          break;
        }
      }
    }
    gurpsItem = this.getOwnedItem(tracked[attr].id);
    gurpsItem.update({ ['data.state']: attrState });
  }
}
