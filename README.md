# gurps4e

Foundry Virtual TableTop support for the Generic Universal Roleplaying System, (any) Edition.

This game aid is intended to provide you with the ability to play GURPS on Foundry VTT but will still require the use of your official GURPS materials.

GURPS is a trademark of Steve Jackson Games, and its rules and art are copyrighted by Steve Jackson Games. All rights are reserved by Steve Jackson Games. This game aid is the original creation of James B Huddleston and is released for free distribution, and not for resale, under the permissions granted in the <a href="http://www.sjgames.com/general/online_policy.html">Steve Jackson Games Online Policy</a>.

Official material for GURPS is published by Steve Jackson Games (http://www.sjgames.com) and may be purchased through Warehouse 23 (http://www.warehouse23.com).

If you would like to support this effort, you may do so here; https://www.patreon.com/jbhuddleston
<h2>User Guide for v3.x</h2>
<p>Almost everything in the system is an Item and so each character sheet begins virtually empty.</p>
<p>There is a script tab that permits you to quickly create a number of items you have stored in the form of an array of JSON objects. The Item types and their format is as follows:</p>
<ul>
<li>Primary-Attribute - These items are processed first for calculation of formulae. They are those attributes that do not depend on the value of any other in your system. This permits a wide variety of house rules or regular options.<pre>
   {
     "name": "Strength",
     "type": "Primary-Attribute",
     "data": {
       "notes": "Your Strength attribute.",
       "abbr": "ST",
       "value": 10
     }
   }
</pre></li>
<li>Hit-Location - An item defining a location that may be hit and/or damaged independently of others.<pre>
  {
    "name": "Left Hand",
    "type": "Hit-Location",
    "data": {
      "notes": "Heavy leather glove",
      "damageResistance": "2",
      "damage": "0",
      "injuryType": "cr",
      "toCripple": ">HP/3",
      "crippled": false,
      "toHitRoll18": "15",
      "toHitRoll6": "2,4,6",
      "toHitPenalty": "-4"
    }
  }
</pre></li>
<li>Trait - Advantages, Disadvantages, Perks and Quirks that you would like to have ready for reference. They have no impact on the function of the system.<pre>
  {
    "name": "Acute Vision",
    "type": "Trait",
    "data": {
      "notes": "You have superior Vision.",
      "category": "advantage"
    }
  }
</pre></li>
<li>Checks - these items permit quick access to rolls against attributes, control numbers such as Bad Temper or conditions such as death, unconsciousness or surprise.<pre>
  {
    "name": "Vision",
    "type": "Rollable",
    "data": {
      "notes": "Perception check based on Vision.",
      "value": 11,
      "formula": "@perception",
      "category": "check"
    }
  }
</pre></li>
<li>Specific Modifier - used to modify a list of target rolls. They can be used to effect the modifiers for Traits, for Talents or anything in between. The target of the modifier is checked to see if its' name starts with one of the comma-separated values in the targets field.<pre>
  {
    "name": "Acute Vision",
    "type": "Modifier",
    "data": {
      "notes": "The modifier associated with the trait Acute Vision.",
      "inEffect": true,
      "value": "1",
      "attack": false,
      "defence": false,
      "success": false,
      "reaction": false,
      "damage": false,
      "primary": false,
      "specific": true,
      "targets": "Vision"
    }
  },
  {
    "name": "Outdoorsman (Talent)",
    "type": "Modifier",
    "data": {
      "notes": "The Outdoorsman (Talent) modifier to the skills: Camouflage, Naturalist, Navigation, Survival, Tracking.",
      "inEffect": true,
      "value": "1",
      "attack": false,
      "defence": false,
      "success": false,
      "reaction": false,
      "damage": false,
      "primary": false,
      "specific": true,
      "targets": "Camouflage, Naturalist, Navigation, Survival, Tracking"
    }
  }
</pre></li>
<li>Equipment - has no real function in the system for now, but allows you to track special items you may have acquired. Making a note on the front tab should do just as well.<pre>
  {
    "name": "Dwarven Warhammer",
    "type": "Equipment",
    "data": {
      "notes": "Fine, Flat side, Pointy side.",
      "quantity": 1,
      "cost": "150cp",
      "weight": 4.25
    }
  }
</pre></li>
<li>Melee-Attack - an item that will permit an attack roll and a damage roll for a specific melee attack. None of the fields are calculated for you so you must edit them to suit the character to whom you have applied the attack. The value will be recalculated by the formula and so, if you just want a static number for the value, enter that number as the formula.<pre>
  {
    "name": "Flat of Dwarven Warhammer",
    "type": "Melee-Attack",
    "data": {
      "notes": "",
      "value": 0,
      "formula": "@axemace",
      "damage": "2d6",
      "damageType": "cr",
      "armourDiv": 1,
      "minST": "11",
      "weight": 4.25,
      "reach": "1"
    }
  }
</pre></li>
<li>Ranged-Attack - an item that will permit an attack roll and a damage roll for a specific ranged attack. None of the fields are calculated for you so you must edit them to suit the character to whom you have applied the attack.<pre>
  {
    "name": "Thrown Dwarven Warhammer",
    "type": "Ranged-Attack",
    "data": {
      "notes": "",
      "value": 0,
      "formula": "@thrown-axemace",
      "damage": "2d6",
      "damageType": "cr",
      "armourDiv": 1,
      "minST": "12",
      "accuracy": "1",
      "range": "14/21",
      "rof": 1,
      "shots": "T(1)",
      "bulk": -4,
      "recoil": ""
    }
  }
</pre></li>
<li>Defence - an item that will permit a roll for a specific Active Defence. Be careful to make the word Parry, Block or Dodge, the first in the name of the defence. This is important for some modifiers that match a string at the beginning of the name.<pre>
  {
    "name": "Parry with Dwarven Warhammer",
    "type": "Defence",
    "data": {
      "notes": "Unbalanced",
      "value": 13,
      "formula": "@axemace /2+3",
      "category": "parry",
      "weight": 4.25
    }
  }
</pre></li>
<li>Skill - an item that will permit a roll against a particular skill.<pre>
  {
    "name": "First Aid",
    "type": "Rollable",
    "data": {
      "notes": "note",
      "value": 11,
      "formula": "@iq",
      "category": "skill"
    }
  }
</pre></li>
<li>Reaction Modifier - a simple modifier to all reaction rolls.<pre>
  {
    "name": "Hideous",
    "type": "Modifier",
    "data": {
      "notes": "Disgusting appearance.",
      "inEffect": false,
      "value": "-4",
      "attack": false,
      "defence": false,
      "success": false,
      "reaction": true,
      "damage": false,
      "primary": false,
      "specific": false,
      "targets": ""
    }
  }
</pre></li>
<li>Defence Modifier - these items apply their value to all active defences when in effect.<pre>
  {
    "name": "Retreat 3",
    "type": "Modifier",
    "data": {
      "notes": "The bonus for retreating while attempting an Active Defence.",
      "inEffect": false,
      "value": "3",
      "attack": false,
      "defence": true,
      "success": false,
      "reaction": false,
      "damage": false,
      "primary": false,
      "specific": false,
      "targets": ""
    }
  }
</pre></li>
<li>Success Modifier - these items apply their value to all success rolls when in effect. There are so many of these that it may be more convenient to use the Global Modifier instead.<pre>
  {
    "name": "Easy Task",
    "type": "Modifier",
    "data": {
      "notes": "The success modifier for tasks with an Easy difficulty.",
      "inEffect": false,
      "value": "+4",
      "attack": false,
      "defence": false,
      "success": true,
      "reaction": false,
      "damage": false,
      "primary": false,
      "specific": false,
      "targets": ""
    }
  },
</pre></li>
<li>Attack Modifier - these items apply their value to all attacks when in effect.<pre>
  {
    "name": "Rapid Strike",
    "type": "Modifier",
    "data": {
      "notes": "The normal penalty to each attack when performing a Rapid Strike.",
      "inEffect": false,
      "value": "-6",
      "attack": true,
      "defence": false,
      "success": false,
      "reaction": false,
      "damage": false,
      "primary": false,
      "specific": false,
      "targets": ""
    }
  },
</pre></li>
<li>Primary Attribute Modifier - these items apply their value to the primary attributes listed when in effect.<pre>
  {
    "name": "Stunned -4",
    "type": "Modifier",
    "data": {
      "notes": "The penalty to your DX and IQ when you have been injured.",
      "inEffect": false,
      "value": "-4",
      "attack": false,
      "defence": false,
      "success": false,
      "reaction": false,
      "damage": false,
      "primary": true,
      "specific": false,
      "targets": "Intelligence, Dexterity"
    }
  },
</pre></li>
<li>Damage Modifier - these items apply their value to all damage rolls when in effect.<pre>
  {
    "name": "AoA Strong",
    "type": "Modifier",
    "data": {
      "notes": "The bonus to damage for an All out Attack - Strong.",
      "inEffect": false,
      "value": "2",
      "attack": false,
      "defence": false,
      "success": false,
      "reaction": false,
      "damage": true,
      "primary": false,
      "specific": false,
      "targets": ""
    }
  },
</pre></li>
</ul>